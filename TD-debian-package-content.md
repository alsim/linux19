# Etude d'un package debian

A partir d'une debian 9 (stretch) fraîchement installé

```bash
root@stretch:~# apt-cache search -f vsftpd
Package: vsftpd
Source: vsftpd (3.0.3-8)
Version: 3.0.3-8+b1
Installed-Size: 345
Maintainer: Keng-Yu Lin <kengyu@lexical.tw>
Architecture: amd64
Replaces: ftp-server
Provides: ftp-server
Depends: debconf (>= 0.5) | debconf-2.0, init-system-helpers (>= 1.18~), libc6 (>= 2.15), libcap2 (>= 1:2.10), libpam0g (>= 0.99.7.1), libssl1.1 (>= 1.1.0), libwrap0 (>= 7.6-4~), adduser, libpam-modules, netbase
Recommends: logrotate, ssl-cert
Conflicts: ftp-server
Description: lightweight, efficient FTP server written for security
Description-md5: 81386f72ac91a5ea48f8db0b023f3f9b
Homepage: http://vsftpd.beasts.org/
Tag: admin::file-distribution, implemented-in::c, interface::daemon,
 network::server, protocol::ftp, protocol::ssl, role::program,
 works-with::file
Section: net
Priority: extra
Filename: pool/main/v/vsftpd/vsftpd_3.0.3-8+b1_amd64.deb
Size: 152542
MD5sum: c63f293f77aae56046e60d1eac8f36e1
SHA256: aac279a5c310c78fa37379285062480d32d966c3e33a51e9209db7ed0a00070d

root@stretch:~#
```

On se créé un rep de travail

```bash
mkdir work ; cd work
```

On récupère le package (le nom du fichier ajouté a l'url du dépot de package 'http://deb.debian.org/debian' )

```bash
root@stretch:~/work# wget -q http://deb.debian.org/debian/pool/main/v/vsftpd/vsftpd_3.0.3-8+b1_amd64.deb
root@stretch:~/work# ls -al
total 160
drwxr-xr-x 2 root root   4096 May 24 18:55 .
drwx------ 3 root root   4096 May 24 18:51 ..
-rw-r--r-- 1 root root 152542 Dec  7  2016 vsftpd_3.0.3-8+b1_amd64.deb
```

On extrait les fichier de l'archive (du package) avec l'utilitaire `ar` installé avec le package `atool`

```bash
root@stretch:~/work# apt-get -q install atool
Reading package lists...
Building dependency tree...
Reading state information...
Suggested packages:
  arc arj lzip lzop nomarch p7zip-full rar rpm unace unalz unrar
The following NEW packages will be installed:
  atool
0 upgraded, 1 newly installed, 0 to remove and 78 not upgraded.
Need to get 0 B/42.0 kB of archives.
After this operation, 142 kB of additional disk space will be used.
Selecting previously unselected package atool.
(Reading database ... 32850 files and directories currently installed.)
Preparing to unpack .../atool_0.39.0-5_all.deb ...
Unpacking atool (0.39.0-5) ...
Setting up atool (0.39.0-5) ...
Processing triggers for man-db (2.7.6.1-2) ...
```

```bash
root@stretch:~/work# ar -x vsftpd_3.0.3-8+b1_amd64.deb
root@stretch:~/work# ls -altr
total 316
-rw-r--r-- 1 root root 152542 Dec  7  2016 vsftpd_3.0.3-8+b1_amd64.deb
drwx------ 3 root root   4096 May 24 18:51 ..
-rw-r--r-- 1 root root      4 May 24 19:01 debian-binary
-rw-r--r-- 1 root root 144636 May 24 19:01 data.tar.xz
-rw-r--r-- 1 root root   7714 May 24 19:01 control.tar.gz
drwxr-xr-x 2 root root   4096 May 24 19:01 .
```

On se créé un sous rep extrait les fichiers control

```bash
root@stretch:~/work# mkdir control ; cd control/
root@stretch:~/work/control# tar zvxf ../control.tar.gz
./
./conffiles
./config
./control
./md5sums
./postinst
./postrm
./prerm
./templates
```

On retrouves les scripts de contrôle, les checksum des fichiers packagés, le fichier control definissant les méta donnée du package..

On décompresse l'archive et on consulte son contenu, c'est bien l'ensemble des fichiers livré avec le package.

```bash
root@stretch:~/work/control# unxz ../data.tar.xz
root@stretch:~/work/control# tar tvf ../data.tar
drwxr-xr-x root/root         0 2016-09-21 08:38 ./
drwxr-xr-x root/root         0 2016-09-21 08:38 ./etc/
-rw-r--r-- root/root       132 2014-05-07 20:17 ./etc/ftpusers
drwxr-xr-x root/root         0 2016-09-21 08:38 ./etc/init.d/
-rwxr-xr-x root/root      2069 2016-08-29 06:04 ./etc/init.d/vsftpd
drwxr-xr-x root/root         0 2016-09-21 08:38 ./etc/logrotate.d/
-rw-r--r-- root/root       126 2014-05-07 20:17 ./etc/logrotate.d/vsftpd
drwxr-xr-x root/root         0 2016-09-21 08:38 ./etc/pam.d/
-rw-r--r-- root/root       319 2014-05-07 20:17 ./etc/pam.d/vsftpd
-rw-r--r-- root/root      5850 2016-09-21 08:38 ./etc/vsftpd.conf
drwxr-xr-x root/root         0 2016-09-21 08:38 ./lib/
drwxr-xr-x root/root         0 2016-09-21 08:38 ./lib/systemd/
drwxr-xr-x root/root         0 2016-09-21 08:38 ./lib/systemd/system/
-rw-r--r-- root/root       248 2014-07-27 09:42 ./lib/systemd/system/vsftpd.service
drwxr-xr-x root/root         0 2016-09-21 08:38 ./usr/
drwxr-xr-x root/root         0 2016-09-21 08:38 ./usr/bin/
-rwxr-xr-x root/root        54 2014-05-07 20:17 ./usr/bin/vsftpdwho
drwxr-xr-x root/root         0 2016-09-21 08:38 ./usr/lib/
drwxr-xr-x root/root         0 2016-09-21 08:38 ./usr/lib/tmpfiles.d/
-rw-r--r-- root/root        41 2015-06-18 14:53 ./usr/lib/tmpfiles.d/vsftpd.conf
drwxr-xr-x root/root         0 2016-09-21 08:38 ./usr/sbin/
-rwxr-xr-x root/root    160008 2016-09-21 08:38 ./usr/sbin/vsftpd
drwxr-xr-x root/root         0 2016-09-21 08:38 ./usr/share/
drwxr-xr-x root/root         0 2016-09-21 08:38 ./usr/share/apport/
drwxr-xr-x root/root         0 2016-09-21 08:38 ./usr/share/apport/package-hooks/
-rw-r--r-- root/root      1059 2016-09-21 08:38 ./usr/share/apport/package-hooks/vsftpd.py
drwxr-xr-x root/root         0 2016-09-21 08:38 ./usr/share/bug/
-rwxr-xr-x root/root      1354 2016-01-16 12:00 ./usr/share/bug/vsftpd
drwxr-xr-x root/root         0 2016-09-21 08:38 ./usr/share/doc/
drwxr-xr-x root/root         0 2016-09-21 08:38 ./usr/share/doc/vsftpd/
-rw-r--r-- root/root      1392 2008-02-02 01:30 ./usr/share/doc/vsftpd/AUDIT
-rw-r--r-- root/root      2908 2008-02-02 01:30 ./usr/share/doc/vsftpd/BENCHMARKS
-rw-r--r-- root/root       822 2012-03-28 03:21 ./usr/share/doc/vsftpd/BUGS
-rw-r--r-- root/root      5091 2011-12-17 19:34 ./usr/share/doc/vsftpd/FAQ.gz
-rw-r--r-- root/root       446 2015-05-25 19:55 ./usr/share/doc/vsftpd/NEWS.Debian.gz
-rw-r--r-- root/root      1361 2015-07-20 22:06 ./usr/share/doc/vsftpd/README
-rw-r--r-- root/root      3179 2014-05-07 20:17 ./usr/share/doc/vsftpd/README.Debian
-rw-r--r-- root/root       112 2008-02-02 01:30 ./usr/share/doc/vsftpd/README.security
-rw-r--r-- root/root      2115 2008-02-02 01:30 ./usr/share/doc/vsftpd/README.ssl
-rw-r--r-- root/root       125 2008-02-02 01:30 ./usr/share/doc/vsftpd/REWARD
drwxr-xr-x root/root         0 2016-09-21 08:38 ./usr/share/doc/vsftpd/SECURITY/
-rw-r--r-- root/root      3129 2009-07-17 20:56 ./usr/share/doc/vsftpd/SECURITY/DESIGN.gz
-rw-r--r-- root/root      2270 2008-02-02 01:30 ./usr/share/doc/vsftpd/SECURITY/IMPLEMENTATION
-rw-r--r-- root/root       486 2008-02-02 01:30 ./usr/share/doc/vsftpd/SECURITY/OVERVIEW
-rw-r--r-- root/root      2534 2008-02-02 01:30 ./usr/share/doc/vsftpd/SECURITY/TRUST.gz
-rw-r--r-- root/root       392 2008-02-02 01:30 ./usr/share/doc/vsftpd/SIZE
-rw-r--r-- root/root      1172 2008-02-02 01:30 ./usr/share/doc/vsftpd/SPEED
-rw-r--r-- root/root      1833 2012-04-05 01:27 ./usr/share/doc/vsftpd/TODO
-rw-r--r-- root/root      1261 2008-02-02 01:30 ./usr/share/doc/vsftpd/TUNING
-rw-r--r-- root/root       221 2016-09-21 08:38 ./usr/share/doc/vsftpd/changelog.Debian.amd64.gz
-rw-r--r-- root/root     12287 2016-09-21 08:38 ./usr/share/doc/vsftpd/changelog.Debian.gz
-rw-r--r-- root/root     27360 2015-07-23 06:01 ./usr/share/doc/vsftpd/changelog.gz
-rw-r--r-- root/root      1668 2016-01-16 10:45 ./usr/share/doc/vsftpd/copyright
drwxr-xr-x root/root         0 2016-09-21 08:38 ./usr/share/doc/vsftpd/examples/
drwxr-xr-x root/root         0 2016-09-21 08:38 ./usr/share/doc/vsftpd/examples/INTERNET_SITE/
-rw-r--r-- root/root      1974 2008-02-02 01:30 ./usr/share/doc/vsftpd/examples/INTERNET_SITE/README.gz
-rw-r--r-- root/root       506 2016-09-21 08:38 ./usr/share/doc/vsftpd/examples/INTERNET_SITE/vsftpd.conf
-rw-r--r-- root/root       533 2008-02-02 01:30 ./usr/share/doc/vsftpd/examples/INTERNET_SITE/vsftpd.xinetd
drwxr-xr-x root/root         0 2016-09-21 08:38 ./usr/share/doc/vsftpd/examples/INTERNET_SITE_NOINETD/
-rw-r--r-- root/root      2043 2008-02-02 01:30 ./usr/share/doc/vsftpd/examples/INTERNET_SITE_NOINETD/README
-rw-r--r-- root/root       564 2016-09-21 08:38 ./usr/share/doc/vsftpd/examples/INTERNET_SITE_NOINETD/vsftpd.conf
drwxr-xr-x root/root         0 2008-02-02 01:30 ./usr/share/doc/vsftpd/examples/PER_IP_CONFIG/
-rw-r--r-- root/root      1498 2008-02-02 01:30 ./usr/share/doc/vsftpd/examples/PER_IP_CONFIG/README
-rw-r--r-- root/root       259 2008-02-02 01:30 ./usr/share/doc/vsftpd/examples/PER_IP_CONFIG/hosts.allow
-rw-r--r-- root/root       815 2008-02-02 01:30 ./usr/share/doc/vsftpd/examples/README
drwxr-xr-x root/root         0 2008-02-02 01:30 ./usr/share/doc/vsftpd/examples/VIRTUAL_HOSTS/
-rw-r--r-- root/root      2160 2008-02-02 01:30 ./usr/share/doc/vsftpd/examples/VIRTUAL_HOSTS/README
drwxr-xr-x root/root         0 2016-09-21 08:38 ./usr/share/doc/vsftpd/examples/VIRTUAL_USERS/
-rw-r--r-- root/root      2444 2016-09-21 08:38 ./usr/share/doc/vsftpd/examples/VIRTUAL_USERS/README.gz
-rw-r--r-- root/root        17 2008-02-02 01:30 ./usr/share/doc/vsftpd/examples/VIRTUAL_USERS/logins.txt
-rw-r--r-- root/root       260 2008-02-02 01:30 ./usr/share/doc/vsftpd/examples/VIRTUAL_USERS/vsftpd.conf
-rw-r--r-- root/root       129 2008-02-02 01:30 ./usr/share/doc/vsftpd/examples/VIRTUAL_USERS/vsftpd.pam
drwxr-xr-x root/root         0 2008-02-02 01:30 ./usr/share/doc/vsftpd/examples/VIRTUAL_USERS_2/
-rw-r--r-- root/root      2003 2008-02-02 01:30 ./usr/share/doc/vsftpd/examples/VIRTUAL_USERS_2/README
drwxr-xr-x root/root         0 2016-09-21 08:38 ./usr/share/lintian/
drwxr-xr-x root/root         0 2016-09-21 08:38 ./usr/share/lintian/overrides/
-rw-r--r-- root/root        75 2014-10-06 08:59 ./usr/share/lintian/overrides/vsftpd
drwxr-xr-x root/root         0 2016-09-21 08:38 ./usr/share/man/
drwxr-xr-x root/root         0 2016-09-21 08:38 ./usr/share/man/man5/
-rw-r--r-- root/root     10924 2016-09-21 08:38 ./usr/share/man/man5/vsftpd.conf.5.gz
drwxr-xr-x root/root         0 2016-09-21 08:38 ./usr/share/man/man8/
-rw-r--r-- root/root      1015 2016-09-21 08:38 ./usr/share/man/man8/vsftpd.8.gz
```
