# TP NFS

## Présentation

NFS : network filesystème

fontionnalité permettant de partager une sous arborescance à partir d'un serveur et de monter celle-ci sur un ou des clients. Ici le serveur partage le dossier /data, celui-ci sera monter sur les deux client dans l'arborescance arbitrairement sur /mnt/data.

![nfs](./images/nfs.png)

## Mise en oeuvre sur centos7

### Prerequis

Les hosts doivent bien pouvoir communiquer au travers du réseaux. On configura un réseaux hostonly commun. et on désactivera le firewall (il est aussi possible d'ouvrir les flux réseaux nfs mais ce n'est pas juste un port tcp)

Il est préférable que les hosts soient synchronisé sur la même date, **on installe donc ntp**(1)

**Les hosts doivent se connaitre par leur noms**(2) cela est plus simple et nécessaire (entre client et serveur) sur certains mode de fontionnement de nfs.

### Installation

Les clients et serveurs doivent disposer du package `nfs-utils` (3)

### Configuration

#### sur le serveur

* on créé les dossiers /data et /readonly
* on configure les **exports** de filesystème dans le fichier /etc/exports et on exporte les sous arborescances :

  ```bash
  [root@localhost ~]# cat /etc/exports
  /data  192.168.33.0/24(rw,async,no_root_squash,no_subtree_check)
  /readonly  192.168.33.0/24(ro,no_root_squash,no_subtree_check)
  [root@localhost ~]# systemctl start nfs-server
  [root@localhost ~]# exportfs
  /data             192.168.33.0/24
  /readonly         192.168.33.0/24
  ```

Vous noterez : les sous-arbo sont exportés (visible) pour tout mon réseaux hostonly, l'export peu être limité à une seule ip, on ajoutera alors un argument par configuration ip suivi par les options d'export :

* ro/rw pour read-only ou read-write
* async permet d'améliorer les performances en validant les écritures avant qu'elle soit réélement écrite sur disque
* no_root_squash : désactive l'interpretation de l'uid de root sur le client en l'uid de nobody sur le serveur (root n'est pas root au travers de nfs si cette option n'est pas spécifié)
* no_subtree_check réduit la charge du serveur en limitant les validation d'accès lorsque la sous arboressance n'est pas un filesystem complet mais une sous arbo d'un file system.

#### Sur les clients

Nous effectuons simplement le montage nfs :

```bash
[root@localhost ~]# mount -t nfs server:/data /mnt/data
[root@localhost ~]# mount | grep /data
server:/data on /mnt/data type nfs4 (rw,relatime,vers=4.1,rsize=65536,wsize=65536,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,clientaddr=192.168.33.11,local_lock=none,addr=192.168.33.10)
[root@localhost ~]# umount server:/data
[root@localhost ~]# mount | grep /data
```

avec la commande mount sans argument il est possible de consulter les options de montage.

(man nfs pour les étudier)

```bash
[root@localhost ~]# mount -t nfs server:/data /mnt/data -o rw,relatime,rsize=65536,wsize=65536,namlen=255,soft,proto=udp,timeo=100,retrans=2,sec=sys,clientaddr=192.168.33.11,local_lock=none,addr=192.168.33.10
[root@localhost ~]# mount | grep /data
server:/data on /mnt/data type nfs (rw,relatime,vers=3,rsize=32768,wsize=32768,namlen=255,soft,proto=udp,timeo=100,retrans=2,sec=sys,mountaddr=192.168.33.10,mountvers=3,mountport=20048,mountproto=udp,local_lock=none,addr=192.168.33.10)
[root@localhost ~]#
```
