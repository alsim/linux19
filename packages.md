# Gestion de packages

## Environement de travail

Afin d'illustrer ce cours, vous devez disposer d'une centos fraichement installé (cf [Installation et configuration d'une centOS 7](./TD-install-centos.md)).

## Présentation des packages logiciels

Une [distribution](./presentation_Linux.md#une-distribution-linux) est une suite logiciel qui intègre un système d'exploitation et un ensemble de logiciel intégré pour celui-ci. **Les packages sont les unités de livraison des logiciels intégrés à la distribution.**

Un package est alors un fichier contenant le produit qu'il délivre ainsi que les éléments permettant son **intégration sur la distribution associé** :

* L'ensemble des fichiers associés au logiciel qu'il contiens, a savoir possiblement :
  * Les exécutable (/usr/bin /usr/sbin)
  * Les bibliothèque partagée (/usr/lib)
  * Des fichier de configuration (/etc/xxxx/xxxx.conf)
  * La documentations associé (page de man et autres)
* Les fichiers permettant son intégration sur le système :
  * Des fragments de l'arborescance supplémentaire
  * Les scriptes permettant leur installation
  * Eventuelement des fichiers d'environnements (/etc/profile.d)
  * Les fichiers permettant leur intégration sur le système et d'éventuel autre logiciel (/etc/yyyy.d/xxxx).

Exemple : l'installation du serveur ftp vsftpd sur la centos:

Installation :

```bash
[root@myhost ~]# yum install vsftpd
Modules complémentaires chargés : fastestmirror
Determining fastest mirrors
 * base: mirror.hostup.org
 * extras: mirror.hostup.org
 * updates: mirroir.wptheme.fr
base                                                         | 3.6 kB  00:00:00
extras                                                       | 2.9 kB  00:00:00
updates                                                      | 2.9 kB  00:00:00
(1/4): base/7/x86_64/group_gz                                | 153 kB  00:00:00
(2/4): extras/7/x86_64/primary_db                            | 194 kB  00:00:00
(3/4): updates/7/x86_64/primary_db                           | 1.3 MB  00:00:02
(4/4): base/7/x86_64/primary_db                              | 6.1 MB  00:00:05
Résolution des dépendances
--> Lancement de la transaction de test
---> Le paquet vsftpd.x86_64 0:3.0.2-27.el7 sera installé
--> Résolution des dépendances terminée

Dépendances résolues

====================================================================================
 Package           Architecture      Version                  Dépôt           Taille
====================================================================================
Installation :
 vsftpd            x86_64            3.0.2-27.el7             base            172 k

Résumé de la transaction
====================================================================================
Installation   1 Paquet

Taille totale des téléchargements : 172 k
Taille d'installation : 353 k
Is this ok [y/d/N]: y
Downloading packages:
vsftpd-3.0.2-27.el7.x86_64.rpm                               | 172 kB  00:00:00
Running transaction check
Running transaction test
Transaction test succeeded
Running transaction
  Installation : vsftpd-3.0.2-27.el7.x86_64                                     1/1
  Vérification : vsftpd-3.0.2-27.el7.x86_64                                     1/1

Installé :
  vsftpd.x86_64 0:3.0.2-27.el7

Terminé !
```

Contenu :

* le programme vsftpd

  ```bash
  [root@myhost ~]# rpm -ql vsftpd | grep bin
  /usr/sbin/vsftpd
  ```

* Les fichiers de config pour le produit et pour logrotate et un fichier d'intégration sur pam (Plugable Authentification Module) :

  ```bash
  [root@myhost ~]# rpm -ql vsftpd | grep /etc
  /etc/logrotate.d/vsftpd
  /etc/pam.d/vsftpd
  /etc/vsftpd
  /etc/vsftpd/ftpusers
  /etc/vsftpd/user_list
  /etc/vsftpd/vsftpd.conf
  /etc/vsftpd/vsftpd_conf_migrate.sh
  ```

* De la doc : (seul les 4 dernières lignes sont présentés ici)

  ```bash
  [root@myhost ~]# rpm -ql vsftpd | grep share | tail -4
  /usr/share/doc/vsftpd-3.0.2/TUNING
  /usr/share/doc/vsftpd-3.0.2/vsftpd.xinetd
  /usr/share/man/man5/vsftpd.conf.5.gz
  /usr/share/man/man8/vsftpd.8.gz
  ```

* Les éléments de l'arbo /var/ftp et /var/ftp/pub

## Format des packages

Deux format principaux **rpm** et **deb** ; respectivement pour les distribution de type RedHat et de type Debian. Il y en a d'autres bien sur mais ils ne seront pas présentés dans ce document.

### Packages rpm

rpm : RedHat Package Manager

#### Nomenclature package rpm

Principe : $nom$**-**$version$**-**$release$**.**$architecture processeur$**.rpm**

Exemple :

vsftpd-3.0.2-27.el7.x86_64.rpm

Nom du produit : vsftpd
Version du produit : 3.0.2
Release du package : 27.el7
Architecture processeur : x86_64

#### Structure package rpm

C'est une structure spécifique lisible avec l'outil `rpm`, elle contiens :

* Une entête de fichier
* Des Signatures cryptographiques
* Un header de package : les métadonnée du package
* L'archive cpio compressé des fichiers à installer
* Des scripts relatifs à l'installation, la suppression ou les mise à jours du package

L'entête du package :

```bash
[root@myhost ~]# rpm -qi vsftpd
Name        : vsftpd
Version     : 3.0.2
Release     : 27.el7
Architecture: x86_64
Group       : System Environment/Daemons
Size        : 361239
License     : GPLv2 with exceptions
Signature   : RSA/SHA256, ven. 03 avril 2020 23:10:15 CEST, Key ID 24c6a8a7f4a80eb5
Source RPM  : vsftpd-3.0.2-27.el7.src.rpm
Build Date  : mer. 01 avril 2020 06:55:33 CEST
Build Host  : x86-02.bsys.centos.org
Relocations : (not relocatable)
Packager    : CentOS BuildSystem <http://bugs.centos.org>
Vendor      : CentOS
URL         : https://security.appspot.com/vsftpd.html
Summary     : Very Secure Ftp Daemon
Description :
vsftpd is a Very Secure FTP daemon. It was written completely from
scratch.
```

#### Commandes de gestion des packages rpm

* Liste des packages installé : `rpm -qa`  
* Contenu d'un package installé : `rpm -ql vsftpd`  
  à tester aussi sur les packages  
  * coreutils
  * util-linux
* Contenu d'un fichier package : `rpm -qpl $pkg-file`  
* Trouver le package associé un fichier : `rpm -qf $FILE`  
  à tester sur les fichiers :
  * /etc/services
  * /sbin/ip
* Vérification d'un package : `rpm -V $PKG`  
  taille md5sum etc...  
* Consultation des Metadonnées d'un package :  
`rpm -q --info openssh-clients`
* Consultation des Scripts pre/post install : `rpm -q --scripts openssh-server`

> Testez ces commandes !

### Packages debian

#### Nomenclature package debian

exemple : apache_1.3.31-6_i386
Nom du package : apache
Version : 1.3.31-6
Architecture : i386

#### Structure package debian

C'est une archive ar contenant :

* Debian-binary : contiens la version du packaging
* data.tar.gz : archive des fichiers à installer/désinstallé du système
* control.tar.gz : fichiers de controle lié à la gestion du package :
  * Conffile : liste des fichiers de config
  * Control : info sur le package
  * Md5sums : checksum du contenu de data.gz
  * Config : script de configuration
  * Scripts de pre ou post installation et suppresion
    * preinst
    * prerm
    * postinst
    * postrm

[td oscultation d'un package debian](TD-debian-package-content.md)

#### Commandes de gestion des packages debian

* Liste des packages installé : `dpkg -l`  
  à utiliser avec un grep
* Contenu d'un package installé : `dpkg -L $PKG`  
  à utiliser avec un grep
* Contenu d'un package : `dpkg -c $PKG`
  à tester sur les packages :
  * coreutil
  * util-linux
  * netbase
* Package owner d'un fichier : `dpkg -S $FILE`
  à tester sur les fichiers :
  * /etc/services
  * /bin/ip
  * /sbin/ifconfig
* reconfiguration d'un package installé : `dpkg --reconfigure`
  Sauvegarde des fichiers de configuration locale, execution de la configuration et la post installation.
* `dpkg --repack` : reconstruit un package à    partir de fichiers locaux (sauvegarde du produit et de sa conf)

## Dépendances

Certains logiciel nécessite que d'autre logiciels soit aussi installés, par exemple une application web nécessitera qu'un serveur web soit déja installé, cette relation s'appele une dépendance.

D'autre logiciels peuvent être incompatible entre eux du fait de leur intégration. Exemple : deux serveurs web configuré pour écouté sur le port 80 ne peuvent pas être lancés en même temps. Etant intégrer pour démarrer automatiquement, ils sont incompatibles, chacun des deux rend l'autre obsolète.

Ainsi pour installer un package il faut parfois necessairement installer ou désinstaller d'autre packages. Les éditeurs de distribution proposent en général des **dépot de packages** et un outil de gestions des dépots de package et des dépendances.

aussi lorsque que l'on utilise un tel outil pour installer un logiciel cet outil étudira les dépendance et proposera d'installer l'ensemble des package necessaire au bon fonctionnement du logiciel demandé.

### yum : YellowDog Update Manager

C'est l'outils de gestion des dépots de RedHat :

Installation d'un package et de ses dépendances : `yum install $PKG`

Supression d'un package : `yum remove $PKG`

Mise à jour système : `yum update`

#### Dépots de package rpm

Certains logiciels proposent d'ajouter tout un depot de packages intégré pour une distribution.
On augmente alors la distribution avec ce dépot
Tecniquement un fichier d"crivant le répository est ajouté dans /etc/yum.repos.d/*

Dans ce fichier on viens definir des canaux. Un canal est en fait un ensemble cohérent packages sur un dépots accessible via un URL.

```bash
 grep -v "^#" /etc/yum.repos.d/CentOS-Base.repo | grep . | head -5
[base]
name=CentOS-$releasever - Base
mirrorlist=http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=os&infra=$infra
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
```

Ici une clef permet de valider la signature numérique des packets.

### apt

C'est l'outils de gestion des dépots de package pour les distribution debian et associés.

Installation : `apt-get install $PKG`  

Supression : `apt-get remove $PKG` / `apt-get --purge remove $PKG`  

Mise à jour de la base locale de package : `apt-get update` (resynchronisation de l'index des packages : /var/lib/dpkg/available)  

Mise à jour système `apt-get upgrade`

#### Dépots de package debian

voir /etc/apt/sources.list et les fichier /etc/apt/sources.list.d/*

chaque fichier deffini des canaux :

* Un Type de canal : deb ou deb-src (packages compilé ou package de code source à compiler)
* L'Url : la racine de dépot
* des Tag : distribution / type de package main contrib non-free

## Depots de packages ajoutés

Les distribution Linux sont fournies avec des dépots de packages officiels configuré afin de permettre l'installation des packages inclue de la distribution.

Certains éditeurs proposent leur propre dépot de package. On ajoute sur le systeme ce dépot afin de disposer des logicels supplémentaires. On installe le logiciel avec le gestionaire de dépots. Il sera aussi possible de le maintenir à jour par ce biais en disposant des mise a jours fournie par l'éditeur du logiciel (plus fréquente) plutot que celles de l'éditeur de la distribution.

Exemple : l'installation officiel de docker propose de désinstaller la version offerte avec la distribution avcant de l'installer avec le dépot maintenu par docker.

Certains des ces dépots sont aussi intégré dans la distribution officiel, exemple le package epel-release sur une centos installe le dépot logiciel EPEL.
