# troubleshooting

C'est notre activité principale : résoudre les incidents et problemes.

> Notez : résoudre un incident consiste à remettre le service en fonction et uniquement cela, traiter le probleme consiste à cherche la cause de l'incident et de proposer des solution à long terme.

## Anomalie courante et connues

- Service arreté
- Saturation de ressources : réseaux disque cpu ram
- regression suite à un changement

## Principe de traitement d'une anomalie inconnue

### Qualification

Il s'agit de definir précisément quel est l'anomalie rencontré, __vérifier qu'il s'agit bien d'une anomalie__ : le service à-t-il déja été fonctionnel, puis lorsque cela est possible de trouver une méthode de reproduction de l'anomalie.

### Analyse

Collecte d'informations et **des traces** en suivant le protocole de reproduction de l'anomalie ou l'horodatage de l'anomalie déclarée.

En reprenant les principes de fonctionnement du service impacté et en validant point par point les éléments rencontrés. Pour le réseaux : les couches du modèle OSI pour la connextivité réseaux mais aussi toutes les couches de l'infrastructure Exemple :

- Resolution dns du service (on récupère l'ip)
- nat vers l'ip privé du service
- reverse proxy ou repartiteur de charge qui porte l'ip du service
- les hosts qui porte le service derière le répartiteur
- les services utilisés par ces hosts

### Correction

horsmis les actions simple de déblocage de la production (redémarrage de service par exemple)
La correction repecte les principes de la gestion des changement en production, dans l'objectif de maitriser l'impact sur la production.

après avoir valider la non regression dans un environment hors production, vous vous preparrer à exécuter l'opération en production.

Celle-ci sera planifier en concertation avec la maitrise d'ouvrage.

> Nous sommes souvent amené à travailler sur un environnement que nous ne maitrisons pas. Technologie non maitrisé, environnement spécifique etc...
>
> Avec sang froid nous pouvons par oportunisme consulter :
>
> - L'historique des commandes de root
> - L'outil de ticketing (checher si cette anpmalie n'as pas déjà eu lieu)
